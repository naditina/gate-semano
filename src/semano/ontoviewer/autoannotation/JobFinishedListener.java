/**
 * 
 */
package semano.ontoviewer.autoannotation;

/**
 * @author naddi
 *
 */
public interface JobFinishedListener {
  
  public void notifyJobFinished();

}
