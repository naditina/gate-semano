package semano.rulebaseeditor;

public interface StoreModificationListener {

   void storeChanged();

}
